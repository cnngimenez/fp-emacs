;;; fp-view.el --- View decs in a buffer

;; Copyright 2020 poo@juno
;;
;; Author: poo@juno
;; Version: $Id: fp-view.el,v 0.0 2020/12/03 18:44:29  Exp $
;; Keywords: 
;; X-URL: not distributed yet

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; 

;; Put this file into your load-path and the following into your ~/.emacs:
;;   (require 'fp-view)

;;; Code:

(provide 'fp-view)
(require 'fp-structs)

(defconst fp-view-buffer-name "*FP*"
  "The buffer name.") ;; defconst

(defvar fp-view-buffer nil
  "Store the buffer object.")

(defun fp-view-show-buffer ()
  "Shot the FP buffer.  Create it if needed.
Return the buffer FP."
  (setq fp-view-buffer (get-buffer-create fp-view-buffer-name))
  (display-buffer-in-direction fp-view-buffer '((direction . leftmost)
						(window-width . 0.2)) )
  (with-current-buffer fp-view-buffer
    (org-mode))
  fp-view-buffer) ;; defun

(defun fp-view-insert-dec (dec &optional buffer)
  "Insert this fpdec DEC structure in a formatted string."
  (insert  "- "
	   "[[file:" (if (fpdec-file dec)
			 (fpdec-file dec)
		       (buffer-file-name buffer))
	   "::"
	   (number-to-string (fpdec-line dec)) "][" (fpdec-name dec) "]] "
	   
	   (fpdec-args dec)
	   "\n") ) ;; defun

(defun fp-view-insert-pack (packname)
  "Insert the pack name. "
  (insert "** " packname "\n") ) ;; defun

(defun fp-view-insert-file (filename)
  "Insert the file name."
  (insert "* Current: [["
	  (when filename filename)
	  "]["
	  (when filename (file-name-nondirectory filename))
	  "]] \n") ) ;; defun


(defun fp-view-show-decs (lstdecs &optional reset file)
  "Display the LSTDECS declarations in a summarized way.
If RESET is t, then delete the buffer content.
If FILE is nil, then consider it is the current buffer file."
  (let ((codebuffer (current-buffer)))
    (with-current-buffer (fp-view-show-buffer)
      (when reset
	(delete-region (point-min) (point-max)))

      (fp-view-insert-file (if file file
			     (buffer-file-name codebuffer)))
      
      (dolist (pack (fp-structs-group-decs-by-packs lstdecs))
	(fp-view-insert-pack (car pack))
	(dolist (dec (cadr pack))
	  (fp-view-insert-dec dec codebuffer))))) ) ;; defun


;;; fp-view.el ends here
