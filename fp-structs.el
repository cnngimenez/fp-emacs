;;; fp-structs.el --- 

;; Copyright 2020 cnngimenez
;;
;; Author: cnngimenez
;; Version: $Id: fp-structs.el,v 0.0 2020/11/28 02:17:06  Exp $
;; Keywords: 
;; X-URL: not distributed yet

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; 

;;; Code:

(provide 'fp-structs)

(defstruct fpdec package name args desc line file)
(defstruct fppack name (line :type integer))
(defstruct fpfile path date)

(defun fp-structs-get-packs-from-decs (lstdecs)
  "Retrieve all packages from the fpdec instances in LSTDECS."
  (delete-dups (mapcar 'fpdec-package lstdecs))
  ) ;; defun

(defun fp-structs-get-decs-with-pack (lstdecs package-name)
  "Return all the fpdec in LSTDECS with the given package name."
  (seq-filter (lambda (dec)
		"Returns True iff the dec has the PACKAGE-NAME."
		(equal (fpdec-package dec) package-name))
	      lstdecs)
  ) ;; defun


(defun fp-structs-group-decs-by-packs (lstdecs)
  "Group LSTDECS by pack groups."
  (mapcar
   (lambda (package-name)
     "Just give the format '(PACKAGE-NAME . list-of-filtered-fpdecs)"
     (cons package-name
	   (list (fp-structs-get-decs-with-pack lstdecs package-name))))
   (fp-structs-get-packs-from-decs lstdecs)
   ) 
  ) ;; defun



;;; fp-structs.el ends here
