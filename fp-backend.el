;;; fp-backend.el --- 

;; Copyright 2020 cnngimenez
;;
;; Author: cnngimenez
;; Version: $Id: fp-backend.el,v 0.0 2020/11/27 14:29:19  Exp $
;; Keywords: 
;; X-URL: not distributed yet

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;; Commentary:

;;; Code:

(provide 'fp-backend)
(require 'fp-structs)

(defcustom fp-backend-binaries-dir "~/.local/share/fp/bin"
  "Where are the binaries of fp?"
  :group 'fp)

(defconst fp-backend-binaries-list
  '(
    (scanfile . "fp_scanfile")
    )
  "What are the binaries names and their commands?"
  ) ;; defconst

(defconst fp-backend-scanlist-pack-regexp
  "^\\([[:digit:]]+\\) | Package: \"\\(.*\\)\"$"
  "Regexp used to parse the package-type scanlist results
Groups:
1: Line number
2: Package name"
  ) ;; defconst

(defconst fp-backend-scanlist-dec-regexp
  "^\\([[:digit:]]+\\) | \\(.*\\) || \\(.*\\) || \\(.*\\) ||: \\(.*\\) ||$"
  "Regexp used to parse the dec-types scanlist results.
Groups:
1: Line number
2: Package/class name
3: Declaration name
4: Parameters
5: Description"
  ) ;; defconst


(defun fp-backend--parse-line (strline)
  "Parse a single line into a fpdec or fppack struct."
  (cond

   ((string-match fp-backend-scanlist-pack-regexp strline)
    (make-fppack :name (match-string 2 strline)
		 :line (string-to-number (match-string 1 strline))))
   
   ((string-match fp-backend-scanlist-dec-regexp strline)     
    (make-fpdec :package (match-string 2 strline)
		:name (match-string 3 strline)
		:args (match-string 4 strline)
		:desc (match-string 5 strline)
		:line (string-to-number (match-string 1 strline))
		:file nil))

   (t nil))
  ) ;; defun

(defun fp-backend--parse-scanfile ()
  "Parse the output of the scanfile program.
The output is in the current buffer.
Return an list of struct instances from the  parsed results."
  (remove 'nil
	  (mapcar 'fp-backend--parse-line
		  (split-string (buffer-string) "\n" t))) ) ;; defun


(defun fp-backend--scanfile-cmd ()
  "Return the command to call the scanfile properly."
  (concat fp-backend-binaries-dir "/"
	   (alist-get 'scanfile fp-backend-binaries-list)) ) ;; defun

(defconst fp-backend-process-buffer-name " *fp-backend*"
  "The backend buffer name."  ) ;; defconst


(defun fp-backend-scanfile (language filepath &optional project)
  "Scan a file and return the results.
If project is nil, then scan without saving to the store."
  (with-current-buffer (get-buffer-create fp-backend-process-buffer-name)
    (delete-region (point-min) (point-max))
    (call-process (fp-backend--scanfile-cmd)
		  nil t nil
		  ;; Program args:
		  (if (null project)
		      "--nosave"
		    project)
		  language
		  filepath)
    (fp-backend--parse-scanfile)
    )
  ) ;; defun



;;; fp-backend.el ends here
