;;; fp.el --- FP Main file

;; Copyright 2020 cnngimenez
;;
;; Author: cnngimenez
;; Version: $Id: fp.el,v 0.0 2020/12/04 01:59:33  Exp $
;; Keywords: 
;; X-URL: not distributed yet

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; 

;; Put this file into your load-path and the following into your ~/.emacs:
;;   (require 'fp)

;;; Code:

(provide 'fp)
(require 'fp-view)
(require 'fp-backend)

(defcustom fp-modes-languages-configs
  '(("prolog" . ((renames . "prologlang")))
    ("ada")
    ("emacs-lisp". ((renames . "elisp"))))
  "List of languages supported and their configurations.

Values should be a alist of languages with their configurations. For instance:

    '((\"prolog\" . ((renames . \"prologlang\"))) (\"ada\"))

It enables two languages: 

- When prolog-mode is enabled, it is interpreted as
  \"prolog\" language but the language parameter used for the backend process
  will be \"prologlang\". 
- The ada-mode is \"ada\" language, and the backend process uses a \"ada\" as
  its parameter (no renames)."
  :group 'fp) ;; defcustom

(defun fp-language (buffer)
  "Return the language associated to the BUFFER.
The language is deduced from the major mode and the 
`fp-modes-languages-configs' renames.  If it is not present in the variable
then it returns nil."
  (with-current-buffer buffer
    (let* ((pair (assoc (string-trim-right (symbol-name major-mode) "-mode")
			fp-modes-languages-configs))
	   (rename (alist-get 'renames (cdr pair))))
      (if rename rename (car pair)))) ) ;; defun

(defun fp-scan-buffer-file (&optional buffer)
  "Scan the BUFFER saved file for declarations and show them.
If BUFFER is nil, then scan the current buffer."
  (interactive)
  (let ((the-buffer (if buffer buffer (current-buffer))))
    (when (and (buffer-file-name the-buffer)
	       (fp-language the-buffer))
      ;; there's a file associated.
      (fp-view-show-decs
       (fp-backend-scanfile (fp-language the-buffer)
			    (buffer-file-name the-buffer))
       t))) ) ;; defun

(add-hook 'after-save-hook #'fp-scan-buffer-file)
(add-hook 'find-file-hook #'fp-scan-buffer-file)

;;; fp.el ends here
